#!/bin/bash
set -x

mkdir -p $CI_PROJECT_DIR/source
cd $CI_PROJECT_DIR/source

# Fetch openttd source
curl --fail -o $CI_PROJECT_DIR/source/openttd.tar.xz "https://cdn.openttd.org/openttd-releases/${OPENTTD_VERSION}/openttd-${OPENTTD_VERSION}-source.tar.xz"

tar -xf $CI_PROJECT_DIR/source/openttd.tar.xz
cd $CI_PROJECT_DIR/source/openttd-${OPENTTD_VERSION}
mkdir build && cd build

# Build with release arguments
if [ $BUILD_TYPE == "client" ]; then
    cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo ..
fi

# Build dedicated server with release arguments
if [ $BUILD_TYPE == "dedicated" ]; then
    cmake -DOPTION_DEDICATED=ON -DCMAKE_BUILD_TYPE=RelWithDebInfo ..
fi

make
mkdir -p $CI_PROJECT_DIR/release

# Move built objects to release directory
cp -R ai baseset game lang openttd $CI_PROJECT_DIR/release

# Zip for artifact
cd $CI_PROJECT_DIR
zip -r $CI_PROJECT_DIR/openttd-$BUILD_TYPE-arm.zip release/
