# openttd-arm
This project simply builds the [OpenTTD](https://www.openttd.org/) client and dedicated server linux binaries for `arm` architecture.

It additionally provides a container image in order to run a dedicated server quickly and easily.

[[_TOC_]]

## Overview
---
> The binaries come **without** OpenGFX. You will need to source it from [here](https://www.openttd.org/downloads/opengfx-releases/latest)
* Tagged versions are formatted `{arch}-{OpenTTD_Version}-{OpenGFX_version}`
* Each tag builds a container image and pushes to project container registry
* Each tag has the following artifacts:
    * `openttd-client-arm.zip`
    * `openttd-dedicated-arm.zip`

## Usage

### Client
>  By default it looks at ~/.openttd for configuration data
* Download `openttd-client-arm.zip` from the tagged version you want.
* Unzip archive
* Run `release/openttd`

### Server
* Download `openttd-dedicated-arm.zip` from the tagged version you want
* Unzip archive
* Follow [the guide](https://wiki.openttd.org/en/Manual/Dedicated%20server) on configuring and running a dedicated server

### Container Image
> The image comes packaged with OpenGFX and runs as `openttd` user
* Pull container image based on tagged version
* Run container with volume mounted to `/home/openttd/.openttd` that contains save games and configuration
* Expose port 3979 from the container

## Roadmap
### Binaries
* Windows ARM Binaries
### Container features
* Save game management
* RCON wrapper for exec commands